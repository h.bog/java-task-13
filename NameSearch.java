public class NameSearch {
	public static void main(String[] args){
		String searchName = "";
		boolean gotResult = false;
		String[] contactList = {"Rose Howells", "Abbie King", "Tommy Warner", "Imogen Bright", "Tom Warren"};
		if (args.length == 0) {	
			System.out.println("Write a name");
		} else {
            for (String elem : args){
                searchName += elem + " ";
            }
            searchName = searchName.substring(0, searchName.length()-1);
            System.out.println("Results:");

            for (String contact : contactList) {
            	if (contact.toLowerCase().contains(searchName.toLowerCase())) {
            		System.out.println(contact);
            		gotResult = true;
            	}
            }
            if (!gotResult) {
            	System.out.println("Got no results when searching for: " + searchName);
            }
		}
	}
}